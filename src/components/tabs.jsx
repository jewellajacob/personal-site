import React, { Component} from 'react';
import AboutMe from "./aboutme";
import Resume from "./resume";
import Projects from "./projects";
import FindGame from "./findgame";
import './tabs.css';

class Tabs extends Component {
  constructor(props) {
        super(props);
        this.toggleClass= this.toggleClass.bind(this);
        this.state = {
            firstActive: true,
            secondTab: false,
            thirdTab: false
        };
    }

  toggleClass(tab) {
    let firstTab = null;
    let secondTab = null;
    let thirdTab = null;
    let fourthTab = null;
    if (tab === 1) {
      firstTab = true;
      secondTab = false;
      thirdTab = false;
      fourthTab = false;
    }
    else if (tab === 2) {
      firstTab = false;
      secondTab = true;
      thirdTab = false;
      fourthTab = false;
    }
    else if (tab === 3) {
      firstTab = false;
      secondTab = false;
      thirdTab = true;
      fourthTab = false;
    }
    else if (tab === 4) {
      firstTab = false;
      secondTab = false;
      thirdTab = false;
      fourthTab = true;
    }
    this.setState({
      firstActive: firstTab,
      secondActive: secondTab,
      thirdActive: thirdTab,
      fourthActive: fourthTab
    });
  };

  render() {
    return(
      <div className="MainContainer">
        <div className="TabContainer">
          <div
            onClick={() => this.toggleClass(1)}
            className={this.state.firstActive ? 'ActiveTab': 'Tab'}
          >
            About
          </div>
          <div
            onClick={() => this.toggleClass(2)}
            className={this.state.secondActive ? 'ActiveTab': 'Tab'}
          >
            Résumé
          </div>
          <div
            onClick={() => this.toggleClass(3)}
            className={this.state.thirdActive ? 'ActiveTab': 'Tab'}
          >
            Projects
          </div>
          <div
            onClick={() => this.toggleClass(4)}
            className={this.state.fourthActive ? 'ActiveTab': 'Tab'}
          >
            Find Game
          </div>
        </div>
        <div className={this.state.firstActive ? '': 'DisplayNone'}>
          <AboutMe></AboutMe>
        </div>
        <div className={this.state.secondActive ? '': 'DisplayNone'}>
          <Resume></Resume>
        </div>
        <div className={this.state.thirdActive ? '': 'DisplayNone'}>
          <Projects></Projects>
        </div>
        <div className={this.state.fourthActive ? '': 'DisplayNone'}>
          <FindGame></FindGame>
        </div>
      </div>
    );
  }


}

export default Tabs;
