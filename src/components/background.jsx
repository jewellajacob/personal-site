import React, { Component } from 'react';
import Tabs from "./tabs";
import "./background.css";

class Background extends Component {

  render() {
    return (
      <div>
        <div className="TitleContainer">
          <h2 className="TitleHeadText">Jacob Jewell</h2>
        </div>
        <Tabs/>
      </div>
    );
  }
}

export default Background;
