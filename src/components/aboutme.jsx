import React, { Component } from 'react';
import "./projects.css";
import "./project.css";
import "./resume.css";
import Pdf from "../documents/jacobjewell.pdf";

class AboutMe extends Component {

  render() {
    return (
      <div className="ProjectsContainer">
        <p className="ResumeDivide">
          About Me:
        </p>
        <p className="ProjectMinorText">
          I am a Computer Science Major with a Mathematics Minor at Central Michigan University. Currently, I am working as a Spring Boot and Angular developer at Ford Motor Company where I am helping to bring Ford development processes to the cloud. In my free time I have made projects that attempt to remove the limitations caused by disabilites.
        </p>
        <p className="ResumeDivide">Contact Information:</p>
        <p className="ProjectMajorText">Phone Number:</p>
        <p className="ProjectMinorText">(989) 350-9196</p>
        <p className="ProjectMajorText">Email:</p>
        <p className="ProjectMinorText">jewel3ja@cmich.edu</p>
        <p className="ResumeDivide">Links and Downloads</p>
        <p className="ProjectMajorText">Project Code:</p>
        <a href="https://gitlab.com/users/jewellajacob/projects" target="_blank" className="ProjectMinorText LinkText">
          gitlab.com/users/jewellajacob/projects
        </a>
        <p className="ProjectMajorText">
          LinkedIn Profile:
        </p>
        <a href="https://www.linkedin.com/in/jacob-jewell-a14256127/" target="_blank" className="ProjectMinorText LinkText">
          www.linkedin.com/in/jacob-jewell
        </a>
        <p className="ProjectMajorText">
          Download Resume:
        </p>
        <a href={Pdf} target="_blank" className="ProjectMinorText LinkText">jacobjewell.pdf</a>
      </div>
    );
  }
}

export default AboutMe;
