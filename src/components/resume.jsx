import React, { Component } from 'react';
import Job from "./job";
import Experience from "./experience";
import "./projects.css";
import "./resume.css";

class Resume extends Component {
  state={
    jobs: [
      {
        id: 1,
        title: "NextGen Software Engineer",
        company: "Ford Motor Company",
        timeFrame: "Jan. 2020 - Present",
        description: "Work within Agile teams as a Java developer to transform the future of Ford’s infrastructure solutions to near instant provisioning of compute, network, database, and cloud services.",
        developmentStack: "Java, Spring Boot, Angular, JavaScript, TypeScript"
      },
      {
        id: 2,
        title: "TechOps Web Developer",
        company: "Central Michigan University",
        timeFrame: "Oct. 2017 - Dec. 2019",
        description: "Maintain and develop resident life sites with a Django framework. Extended focus on backend Python development and database interaction.",
        developmentStack: "Python, Django, React, JavaScript, SQL"
      },
      {
        id: 3,
        title: "Software Engineer Intern",
        company: "Ford Motor Company",
        timeFrame: "Summer 2019",
        description: "Develop automated business processes to support the EU “Right to Be Forgotten” requirement using a Java framework.",
        developmentStack: "Java, Struts"
      }
    ],
    experiences: [
      {
        id: 1,
        title: "LGBTQ Alternative Break",
        timeFrame: "Summer 2018",
        description: "Volunteered at GMHC in New York City to provide assistance to business processes"
      },
      {
        id: 2,
        title: "Adult Special Education Alternative Break",
        timeFrame: "Spring 2017",
        description: "Volunteered at St. Peter's for adult special education in Baltimore"
      },
      {
        id: 3,
        title: "Study Abroad: Honor's Global Citizenship",
        timeFrame: "Summer 2017",
        description: "Studied global citizenship in Germany, Austria, Czech Republic, and Britain under the Honor's Program Director"
      }
    ]
  };

  render() {
    return (
      <div className="ProjectsContainer">
        <p className="ResumeDivide">Education</p>
        <h2 className="ResumeTitle">Computer Science, B.S.</h2>
        <p className="ResumeDate">2016 - 2019</p>
        <p className="ResumeMiscText">Central Michigan University</p>
        <p className="ResumeMiscText">Relevant Coursework:</p>
        <ul className="ResumeMiscText">
          <li>Design Patterns</li>
          <li>Advanced Data Structures and Algorithms</li>
          <li>Assembly Language</li>
          <li>Intro to Artificial Intelligence</li>
          <li>Mathematical Proofs</li>
          <li>Discrete Mathematics</li>
          <li>Virtual Reality</li>
        </ul>
        <p className="ResumeDivide">Work</p>
        { this.state.jobs.map(job => (
          <Job
            key={job.id}
            title={job.title}
            timeFrame={job.timeFrame}
            company={job.company}
            developmentStack={job.developmentStack}
            description={job.description}
          />
        ))}
        <p className="ResumeDivide">Professional Development</p>
        { this.state.experiences.map(experience => (
          <Experience
            key={experience.id}
            title={experience.title}
            timeFrame={experience.timeFrame}
            description={experience.description}
          />
        ))}
      </div>
    );
  }
}

export default Resume;
