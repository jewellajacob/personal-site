import React, { Component } from 'react';
import './project.css'

class Project extends Component{

  render() {
    return (
      <div className='ProjectContainer'>
        <h2 className='ProjectTitle'>{ this.props.title }</h2>
        <p className='ProjectMajorText'>Development Stack:</p>
        <p className='ProjectMinorText'>{ this.props.developmentStack }</p>
        <p className='ProjectMajorText'>Goal:</p>
        <p className='ProjectMinorText'>{ this.props.goal }</p>
        <p className='ProjectMajorText'>Result:</p>
        <p className='ProjectMinorText'>{ this.props.result }</p>
        <p className={this.props.acclaim ? 'ProjectMajorText': 'DisplayNone'}>Acclaim:</p>
        <p className='ProjectMinorText'>{ this.props.acclaim }</p>
      </div>
    );
  }
}

export default Project;
