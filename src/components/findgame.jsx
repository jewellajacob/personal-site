import React, {Component} from 'react';
import Project from "./project";

import "./projects.css";
import "./project.css";
import "./resume.css";
import "./findgame.css";

class FindGame extends Component {

  constructor(props) {
    super(props) ;
    this.state = {
      value: '',
      game : '',
    }

    this.handleChange = this.handleChange.bind(this);
    this.getGame = this.getGame.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  getGame(event) {
    event.preventDefault();
    if (this.state.value !== null || this.state.value !== ''){
      fetch('https://cors-anywhere.herokuapp.com/' + 'http://jewellajacob.pythonanywhere.com/' + this.state.value)
      .then(res => res.json() ) .then(json => {
        console.log(json);
        this.setState ({
          game: json.game,
        })
       });
   }
  }

  render() {
    var{game}= this.state;
    return (

      <div className="ProjectsContainer">
        <div className='ProjectContainer'>
          <h2 className='ProjectTitle'>Find a Steam Game to Play:</h2>
          <form onSubmit={this.getGame}>
            <p className='ProjectMajorText'>Enter Comma Seperated Steam IDs:</p>
            <div className='InputStyle'>
              <input type="text" value={this.state.value} onChange={this.handleChange} />
              <button>Enter</button>
            </div>
          </form>

          {game.length > 0 &&
            <p className='InputStyle'>Play {game}!</p>
          }
        </div>
      </div>
    );
  }

}

export default FindGame;
