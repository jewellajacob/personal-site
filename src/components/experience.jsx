import React, {Component} from 'react';
import "./resume.css";
class Experience extends Component {

  render() {
    return (
      <div>
        <h2 className="ResumeTitle">
          { this.props.title }
        </h2>
        <p className="ResumeDate">
          { this.props.timeFrame }
        </p>
        <p className="ResumeMiscText">
          { this.props.description }
        </p>
      </div>
    );
  }
}

export default Experience;
