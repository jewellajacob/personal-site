import React, {Component} from 'react';
import Project from "./project";
import "./projects.css";

class Projects extends Component {
  state = {
    projects: [
      {
        id: 1,
        title: 'Setting Builder',
        developmentStack: 'Python 3, flask, nltk, spacy',
        goal: 'Create a visual interface from books.',
        result: 'The vision of this application was to create a way to read text from a book and create the setting that is described by the words. In order to accomplish this the project required two components, a natural language processing application and a visual world building application. For the language processing portion, I created a locally run Python Flask API that creates a json structure of the necessary information. For the world building portion, I created a C# Unity project that takes the json and turns it into a visual world. ',
        acclaim: null
      },
      {
        id: 2,
        title: 'VR Drummer',
        developmentStack: 'C#, Unity, Occulus Rift',
        goal: 'Allow those with paralysis below the neck to play the drums.',
        result: 'Within the Unity game engine enviornment I created two playable versions of a drum set. The first version plays the drum set in real time. As the user looks around in virtual reality they will be able to glance at the different pieces of the set in which the instrument will then play. The second version will allow the user to compose music with specific pacing before playing the entire composition at once.',
        acclaim: 'Accepted into the 2018 Meaningful Play Conference and HCII'
      },
      {
        id: 3,
        title: 'Braille Email Box',
        developmentStack: 'Python, Raspberry Pi, RPi.GPIO, sendgrid, Gmail API',
        goal: 'Allow the blind community to communicate in the technological sphere at work.',
        result: 'Through the use of the Raspberry Pi GPIO capabilities I hooked up six solenoids to push up and represent the braille characters. Using the Gmail API, the application pulled in a users new emails and printed out their content to the user through the braille solenoids. Then using the Sendgrid API, the user could send back one of six specfic responses as an email by pressing a button on the Raspberry Pi device.',
        acclaim: null
      },
      {
        id: 4,
        title: 'Film Genre Expert System',
        developmentStack: 'Python, themoviedb API',
        goal: 'Accurately recommend movie genres to a user by asking them questions.',
        result: 'The system goes through a set of questions loaded into the program with json data. Depending on how the questions are answered, more questions will be loaded in to narrow down the suggested genre to one. Then the moviedb api will be used to recommend three random movies of the suggested genre.',
        acclaim: null
      },
      {
        id: 5,
        title: 'Artificial Music Composition',
        developmentStack: 'Python, MIDI database',
        goal: 'Use a method of artificial intelligence to do creative work.',
        result: 'The program takes input from the user to decide the source of inspiration (composer). The algorithm then using the past work of the composer creates a new piano melody from sounds the past composer has used.',
        acclaim: null
      }
    ]
  };

  render() {
    return (
      <div className="ProjectsContainer">
        { this.state.projects.map(project =>(
          <Project
            key={project.id}
            title={project.title}
            developmentStack={project.developmentStack}
            goal={project.goal}
            result={project.result}
            acclaim={project.acclaim}
          />
        ))}
      </div>
    );
  }
}

export default Projects;
